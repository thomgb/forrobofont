

from robofab.tools.toolsAll import readGlyphConstructions as rGC
from AppKit import NSColor
from vanilla import *
from defconAppKit.windows.baseWindow import BaseWindowController
from mojo.events import addObserver, removeObserver
from mojo.drawingTools import *
from mojo.extensions import getExtensionDefault, setExtensionDefault
from mojo.UI import UpdateCurrentGlyphView
from lib.tools.defaults import getDefault, setDefault
from lib.tools.notifications import PostNotification
from mojo.roboFont import CurrentFont, CurrentGlyph
from lib.cells.colorCell import ColorCell
import _glyphConstructionCategories
reload(_glyphConstructionCategories)
from _glyphConstructionCategories import *


title ="BaseGlyph Overlay" # Keep them unique!
event = "draw" 
turnOffItems=[] 
settingsWindow = "%sSettingsKey" % title
defaultKey = "nl.geenbitter.baseGlyphOverlay"

# dont edit
selfKey = "%sKey" % title
wasOn = getExtensionDefault(selfKey)

pijlen = {
	'top': u'\u2191',
	'bottom': unichr(8595),
	'below': unichr(8615),
	'right': u"r",
	'middle': u"m",
	'bar': u"–",
	'ogonek': u'o',
	'cedilla': u'c',
	'horn': u'h',
	}

def getAllAccents(myset):
	allAccents = {}
	data = myset.split("\n")
	for i in data:
		if len(i) == 0: continue
		if i[0] != '#':
			for j in i.split():
				if "." in j:
					allAccents[j.split(".")[0]] = []
	
	for i in data:
		if len(i) == 0: continue
		if i[0] != '#':
			for j in i.split():
				if "." in j:
					allAccents[j.split(".")[0]].append((i.split()[1], j.split(".")[-1]))
					
					allAccents[j.split(".")[0]] = list(set(allAccents[j.split(".")[0]]))

	return allAccents

print getAllAccents(alles)