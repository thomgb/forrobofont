# coding=utf-8



from robofab.tools.toolsAll import readGlyphConstructions as rGC
from AppKit import NSColor
from vanilla import *
from defconAppKit.windows.baseWindow import BaseWindowController
from mojo.events import addObserver, removeObserver
from mojo.drawingTools import *
from mojo.extensions import getExtensionDefault, setExtensionDefault
from mojo.UI import UpdateCurrentGlyphView
from lib.tools.defaults import getDefault, setDefault
from lib.tools.notifications import PostNotification
from mojo.roboFont import CurrentFont, CurrentGlyph
from lib.cells.colorCell import ColorCell
import _glyphConstructionCategories
reload(_glyphConstructionCategories)
from _glyphConstructionCategories import *


title ="ATB_BaseGlyphOverlay" # Keep them unique!
event = "draw" 
turnOffItems=[] 
settingsWindow = "%sSettingsKey" % title
defaultKey = "nl.geenbitter.baseGlyphOverlay"

# dont edit
selfKey = "%sKey" % title
wasOn = getExtensionDefault(selfKey)

pijlen = {
	'top': u'\u2191',
	'bottom': unichr(8595),
	'below': unichr(8615),
	'right': u"r",
	'middle': u"m",
	'bar': u"–",
	'ogonek': u'o',
	'cedilla': u'c',
	'horn': u'h',
	'ring': u'º',
	}

def getAllAccents(myset):
	allAccents = {}
	data = myset.split("\n")
	for i in data:
		if len(i) == 0: continue
		if i[0] != '#':
			for j in i.split():
				if "." in j:
					allAccents[j.split(".")[0]] = []
	
	for i in data:
		if len(i) == 0: continue
		if i[0] != '#':
			for j in i.split():
				if "." in j:
					allAccents[j.split(".")[0]].append((i.split()[1], j.split(".")[-1]))
					
					allAccents[j.split(".")[0]] = list(set(allAccents[j.split(".")[0]]))

	return allAccents
allAccents = getAllAccents(alles)

def getNeededAnchors(myset):
	neededAnchors = {}
	data = myset.split("\n")
	for i in data:
		if len(i) == 0: continue
		if i[0] != '#':
			neededAnchors[i.split()[1]] = []
	for i in data:
		if len(i) == 0: continue
		if i[0] != '#':
			baseGlyph = i.split()[1]
			for j in i.split():
				if "." in j:
					neededAnchors[baseGlyph].append(j.split(".")[-1])
					neededAnchors[baseGlyph] = list(set(neededAnchors[baseGlyph]))

	return neededAnchors

neededAnchorsDict = getNeededAnchors(alles)


def readGlyphConstructionsRF():
	"""read GlyphConstruction and turn it into a dict"""
	
	data = alles.split("\n")
	glyphConstructions = {}
	for i in data:
		if len(i) == 0: continue
		if i[0] != '#':
			name = i.split(': ')[0] #composition
			construction = i.split(': ')[1].split(' ')
			build = [construction[0]]
			for c in construction[1:]:
				accent = c.split('.')[0]
				position = c.split('.')[1]
				build.append((accent, position))
			glyphConstructions[name] = build
	return glyphConstructions

RF_Constuctions = readGlyphConstructionsRF()

def readGlyphConstructions(mySet=alles):
	"""read GlyphConstruction and turn it into a dict"""
	data = mySet.split("\n")
	glyphConstructions = {}
	for i in data:
		if len(i) == 0: continue
		if i[0] != '#':
			name = i.split(': ')[0]
			construction = i.split(': ')[1].split(' ')
			build = [construction[0]]
			for c in construction[1:]:
				accent = c.split('.')[0]
				position = c.split('.')[1]
				build.append((accent, position))
			glyphConstructions[name] = build
	#return glyphConstructions
	reverseGlyphConstructions= dict()
	for accentGlyph, construction in glyphConstructions.items():
		if '.sc' in construction[0]:
			baseGlyph = construction[0].lower()
		else:
			baseGlyph = construction[0]
		
		parts = construction[1:]
		if baseGlyph not in reverseGlyphConstructions:
			reverseGlyphConstructions[baseGlyph] = dict()
		
		for accentGlyph, position in parts:
			if position not in reverseGlyphConstructions[baseGlyph]:
				reverseGlyphConstructions[baseGlyph][position] = set()
			
			reverseGlyphConstructions[baseGlyph][position].add(accentGlyph)
	return reverseGlyphConstructions

Latin_Supp_block = readGlyphConstructions(Latin_Supp)
#print Latin_Supp_block
Latin_Ext_A_block = readGlyphConstructions(Latin_Ext_A)
Latin_Ext_B_block = readGlyphConstructions(Latin_Ext_B)
Latin_Ext_additional_block = readGlyphConstructions(Latin_Ext_additional)

# 
alles_block = readGlyphConstructions(alles)

# dont change name of class
class ThisObserver(BaseWindowController):
		
	def __init__(self, active=bool):
		self.active = active
		if getExtensionDefault(selfKey):
			if self.active == True:
				return
			if self.active == False:
				pass
		
		if self.active == True:
			self._activate()
			self.turnOff()
		if self.active == False:
			self._end()

		
	# dont change names of functions
	# dont edit _functions!
	# but you can add functions if needed


	def _activate(self):	
		setExtensionDefault(selfKey, self)
		addObserver(self, "mainFunction", event)
		addObserver(self, "glyphChange", "currentGlyphChanged")
		addObserver(self, "drawPreviewAccents", "drawPreview")
		addObserver(self, "warn", "draw")


	def _end(self):
		removeObserver(getExtensionDefault(selfKey),event)
		removeObserver(getExtensionDefault(selfKey),"currentGlyphChanged")
		removeObserver(getExtensionDefault(selfKey),"drawPreview")
		removeObserver(getExtensionDefault(selfKey),"draw")

		setExtensionDefault(selfKey, None)
		# this is optional
		if wasOn:
			self.restoreGlyphViewItems()
		#kill settings window
		self.settingsWindow(onOff=False)
	
	def mainFunction(self, info):
		if not getExtensionDefault(settingsWindow):
			self.settingsWindow(onOff=True)
		
		self.w = getExtensionDefault(settingsWindow)	
		self.drawAccents(info)
	def settingsWindow(self, onOff=bool):
		if onOff == True:
			self.w = FloatingWindow((1443.0, 108.0, 160.0, 72.0), 
				title, closable=False)
			setExtensionDefault(settingsWindow, self.w)
			#print getExtensionDefault(settingsWindow)
			columnDescriptions = [
								  dict(title="", key="checkBox", cell=CheckBoxListCell(), width=15),
								  dict(title="baseGlyph",editable=False),
								  dict(title="anchor",width=60,editable=False),


								  ]
			
			self.w.latin_supp = CheckBox((5,2,35,17),"supp",sizeStyle='mini', callback=self.checkOnUnicode)
			self.w.latin_A = CheckBox((40,2,35,17),"ex a",sizeStyle='mini', callback=self.checkOnUnicode)
			self.w.latin_B = CheckBox((75,2,35,17),"ex b",sizeStyle='mini', callback=self.checkOnUnicode)
			self.w.latin_add = CheckBox((110,2,35,17),"add",sizeStyle='mini', callback=self.checkOnUnicode)

			self.w.allOn = SquareButton((0,17,-75,17),"all on", callback=self.allOn, sizeStyle='mini')
			self.w.allOff = SquareButton((75,17,-0,17),"all off", callback=self.allOff, sizeStyle='mini')
			self.w.list = List((0, 17+17, -0, -20),
							   items=[], 
							   columnDescriptions=columnDescriptions,
							   editCallback=self.listEditCallback,
							   )
			
			
			self.setUpBaseWindowBehavior()
			self.w.open()
			self.buildAccentList()
		
		if onOff == False:
			try:
				getExtensionDefault(settingsWindow).hide()
				setExtensionDefault(settingsWindow, None)
			except:
				pass
	
	def warn(self, info):
		glyph = CurrentGlyph()
		glyphName = glyph.name



		if glyphName in neededAnchorsDict:
			nowNeededAnchors = neededAnchorsDict[glyphName]
		else: 
			return
		glyphAnchors = list()
		for anchor in glyph.anchors:
			glyphAnchors.append(anchor.name)
		missing =  list(set(nowNeededAnchors).difference(set(glyphAnchors)))

		fill(1,0,0,.8)
		stroke(None)
		fontSize(40)
		y=50
		for a in missing:
			text("missing anchor: %s" % a,(glyph.width+50,y))
			y+=36

	
	def allOn(self, sender):
		for i in self.w.list.get():
			i['checkBox']=True
	def allOff(self, sender):
		for i in self.w.list.get():
			i['checkBox']=False

	def checkSupp(self, sender):
		
		g=CurrentGlyph()
		defaults = getExtensionDefault(defaultKey, dict())
		# print Latin_Supp_block
		# print
		# print defaults
		# print

		for glyphName in Latin_Supp_block:
			if glyphName == g.name:
				# g.name
				for anchorName in Latin_Supp_block[glyphName]:
					# top
					for accent in Latin_Supp_block[glyphName][anchorName]:
						# grave
						for listItem in self.w.list.get():
							
							for pijl in pijlen.items():
								if listItem["anchor"] == pijl[1]:
									anchor = pijl[0]
									if anchor == anchorName and accent == listItem['accent']:
										listItem['checkBox'] = True
									
	def checkOnUnicode(self, sender):
		if sender == self.w.latin_supp:
			myset = Latin_Supp_block
		if sender == self.w.latin_A:
			myset = Latin_Ext_A_block
		if sender == self.w.latin_B:
			myset = Latin_Ext_B_block
		if sender == self.w.latin_add:
			myset = Latin_Ext_A_block
			myset = Latin_Ext_additional_block
		
		g=CurrentGlyph()
		defaults = getExtensionDefault(defaultKey, dict())
		# print Latin_Supp_block
		# print
		# print defaults
		# print

		for glyphName in myset:
			if glyphName == g.name:
				# g.name
				for anchorName in myset[glyphName]:
					# top
					for accent in myset[glyphName][anchorName]:
						# grave
						for listItem in self.w.list.get():
							
							for pijl in pijlen.items():
								if listItem["anchor"] == pijl[1]:
									anchor = pijl[0]
									if anchor == anchorName and accent == listItem['accent']:
										listItem['checkBox'] = True
									


	def listEditCallback(self, sender):
		## save to defaults
		defaults = getExtensionDefault(defaultKey, dict())
		# print defaults
		for item in sender:

			## anchor position
			for pijl in pijlen.items():
				if item["anchor"] == pijl[1]:
					anchor = pijl[0]
			##
			
			accentPosition = "%s.%s" % (item["baseGlyph"], anchor)
			defaults[accentPosition] = item["checkBox"]
			
		setExtensionDefault(defaultKey, defaults)
		UpdateCurrentGlyphView()
	
	def buildAccentList(self, sender=None):
		try:
			self.w = getExtensionDefault(settingsWindow)
			## welke unicode blokken?
	
			glyph = CurrentGlyph()

			if glyph is None:
				self.w.setTitle("---")
				self.w.list.set([])
				return
			
			font = glyph.getParent()
			
			glyphName = glyph.name
			self.w.setTitle(glyph.name)
			
			items = []
			defaults = getExtensionDefault(defaultKey, dict())
			if glyphName in allAccents.keys():
				#print allAccents[glyphName]
				
				for positions in allAccents[glyphName]:
	
					baseGlyph = positions[0]
					anchor = positions[1]
					
						#if accentGlyph in font:
					#print glyphName, positions[0], positions[1]
					items.append(dict(
						anchor = pijlen[anchor],
						baseGlyph = baseGlyph,
						checkBox = False,
						))
			self.w.list.set(items)
			#print len(items)
			self.w.resize(160,17+17+17+(19*len(items))+21)
		except:
			pass

	
	## notifications
	
	def glyphChange(self, info):
		if info['glyph'].name in allAccents:
			self.buildAccentList()
	
	def drawAccents(self, info):
		
		glyph = info["glyph"]

		
		font = glyph.getParent()
		items = self.w.list.get()

		fill(0.34, 0.71, 0.14, .7)
		stroke(None)
		
		accentAnchors = {}
		for a in glyph.anchors:
			accentAnchors[a.name]=[a.x, a.y]
			
		
		
		for item in items:
			if not item["checkBox"]:
				continue
			
			baseGlyph = item['baseGlyph']
			
			for pijl in pijlen.items():
				if item["anchor"] == pijl[1]:
					anchor = pijl[0]

			if baseGlyph in font:
				for accentAnchor in glyph.anchors:
					for baseGlyphAnchor in font[baseGlyph].anchors:
						if baseGlyphAnchor.name == anchor: ## zoek hier op elk anchor je moet hebben
							if baseGlyphAnchor.name == accentAnchor.name[1:]:
								save()
								## do the transform here
								x = accentAnchor.x - baseGlyphAnchor.x
								y = accentAnchor.y - baseGlyphAnchor.y
								translate(x,y)
								## INDIVIDUELE KLEUR
								#red = float(randint(0,10))/10
								#green = float(randint(0,10))/10
								#blue = float(randint(0,10))/10
								#print red, green, blue
								#fill(red,green,blue,.2)
								##
								drawGlyph(font[baseGlyph])
								restore()
					
	def drawPreviewAccents(self, info):
		glyph = info["glyph"]

		
		font = glyph.getParent()
		items = self.w.list.get()

		fill(0)
		stroke(None)
		
		accentAnchors = {}
		for a in glyph.anchors:
			accentAnchors[a.name]=[a.x, a.y]
			
		
		
		for item in items:
			if not item["checkBox"]:
				continue
			
			baseGlyph = item['baseGlyph']
			
			for pijl in pijlen.items():
				if item["anchor"] == pijl[1]:
					anchor = pijl[0]

			if baseGlyph in font:
				for accentAnchor in glyph.anchors:
					for baseGlyphAnchor in font[baseGlyph].anchors:
						if baseGlyphAnchor.name == anchor: ## zoek hier op elk anchor je moet hebben
							if baseGlyphAnchor.name == accentAnchor.name[1:]:
								save()
								## do the transform here
								x = accentAnchor.x - baseGlyphAnchor.x
								y = accentAnchor.y - baseGlyphAnchor.y
								translate(x,y)
								## INDIVIDUELE KLEUR
								#red = float(randint(0,10))/10
								#green = float(randint(0,10))/10
								#blue = float(randint(0,10))/10
								#print red, green, blue
								#fill(red,green,blue,.2)
								##
								drawGlyph(font[baseGlyph])
								restore()

	def turnOff(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				#global gridWasOn
				#gridWasOn = pref_as_is[i]
				pref_new[i] = 0
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")

	def restoreGlyphViewItems(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				#global gridWasOn
				#gridWasOn = pref_as_is[i]
				pref_new[i] = 1
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")
