from robofab import *
from mojo.UI import *
from mojo.events import addObserver, removeObserver
from mojo.drawingTools import *
from vanilla import *
from lib.tools.defaults import getDefault, setDefault
from lib.tools.notifications import PostNotification
from mojo.extensions import getExtensionDefault, setExtensionDefault
from random import choice
from mojo.roboFont import CurrentFont
from AppKit import NSImageNameRefreshTemplate
from lib.UI.toggleImageButton import ToggleImageButton
title ="Right Glyph" # Keep them unique!
event = "drawBackground" 
turnOffItems=[] 
settingsWindow = "%sSettingsKey" % title

numbers=['1','2','3','4','5','6','7','8','9','0']

# dont edit
selfKey = "%sKey" % title
wasOn = getExtensionDefault(selfKey)
# dont change name of class
class ThisObserver(object):
		
	def __init__(self, active=bool):
		self.active = active
		if getExtensionDefault(selfKey):
			if self.active == True:
				return
			if self.active == False:
				pass
		
		if self.active == True:
			self._activate()
			self.turnOff()
		if self.active == False:
			self._end()

		
	# dont change names of functions
	# dont edit _functions!
	# but you can add functions if needed


	def _activate(self):	
		setExtensionDefault(selfKey, self)
		addObserver(self, "mainFunction", event)
		addObserver(self, "drawPreview", "drawPreview")


	def _end(self):
		removeObserver(getExtensionDefault(selfKey),event)
		removeObserver(getExtensionDefault(selfKey),"drawPreview")

		setExtensionDefault(selfKey, None)
		# this is optional
		if wasOn:
			self.restoreGlyphViewItems()
		#kill settings window
		self.settingsWindow(onOff=False)
	
	def mainFunction(self, info):
		if not getExtensionDefault(settingsWindow):
			self.settingsWindow(onOff=True)
		
		self.w = getExtensionDefault(settingsWindow)	
		fillColor = tuple([ float(i) for i in str(self.w.colorFill.get()).split(' ')[1:] if i ])
		fill(*fillColor)
		stroke(None)

		font = info['glyph'].getParent()

		glyph = self.w.edit.get()
		if len(glyph) == 0:
			return		
		if len(glyph) == 1:
			glyph = font.naked().unicodeData.glyphNameForUnicode(ord(glyph))
		
		if glyph.split(".")[0]:
			try:
				baseGlyphName = font.naked().unicodeData.glyphNameForUnicode(ord(glyph.split(".")[0]))
				glyph = ".".join([baseGlyphName, glyph.split(".")[1]])
			except:
				pass
		try:
			save()
			translate(info['glyph'].width,0)
			drawGlyph(font[glyph])
			restore()
		except:
			print "can't draw that glyph"
			pass
	
	def drawPreview(self, info):
		self.w = getExtensionDefault(settingsWindow)
		font = info["glyph"].getParent()

		fill(0, 0, 0)
		stroke(None)

		glyph = self.w.edit.get()
		if len(glyph) == 0:
			return		
		if len(glyph) == 1:
			glyph = font.naked().unicodeData.glyphNameForUnicode(ord(glyph))
		
		if glyph.split(".")[0]:
			try:
				baseGlyphName = font.naked().unicodeData.glyphNameForUnicode(ord(glyph.split(".")[0]))
				glyph = ".".join([baseGlyphName, glyph.split(".")[1]])
			except:
				pass
		try:
			save()
			translate(info['glyph'].width,0)
			drawGlyph(font[glyph])
			restore()
		except:
			print "can't draw that glyph"
			pass

	def settingsWindow(self, onOff=bool):
		if onOff == True:
			self.w = FloatingWindow((100,70),title, closable = False)
			setExtensionDefault(settingsWindow, self.w)
			
			f = CurrentFont().keys()
			self.w.edit = EditText((7,7,-7,23), choice(f), callback=self.updateView)
			self.w.colorFill = ColorWell((7,37,-40,23))
			self.w.colorFill.set(NSColor.colorWithCalibratedRed_green_blue_alpha_(1.0, 0.23, 0.11,.7))
			
			self.w.randomGlyph = ToggleImageButton((64,37,-7,23), '', bordered=False, imageNamed=NSImageNameRefreshTemplate,callback=self.randomGlyph)

			self.w.open()

		if onOff == False:
			try:
				getExtensionDefault(settingsWindow).hide()
				setExtensionDefault(settingsWindow, None)
			except:
				pass
	
	def randomGlyph(self, sender):
		f = CurrentFont().keys()
		self.w.edit.set(choice(f))
		self.updateView(None)


	def updateView(self, sender):
		PostNotification('doodle.updateGlyphView')


	def turnOff(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				#global gridWasOn
				#gridWasOn = pref_as_is[i]
				pref_new[i] = 0
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")

	def restoreGlyphViewItems(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				#global gridWasOn
				#gridWasOn = pref_as_is[i]
				pref_new[i] = 1
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")