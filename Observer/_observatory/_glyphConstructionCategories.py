Testing =""""""


Latin_Supp="""Agrave: A grave.top
Aacute: A acute.top
Acircumflex: A circumflex.top
Atilde: A tilde.top
Adieresis: A dieresis.top
Aring: A ring.ring
Ccedilla: C cedilla.cedilla
Egrave: E grave.top
Eacute: E acute.top
Ecircumflex: E circumflex.top
Edieresis: E dieresis.top
Igrave: I grave.top
Iacute: I acute.top
Icircumflex: I circumflex.top
Idieresis: I dieresis.top
Ntilde: N tilde.top
Ograve: O grave.top
Oacute: O acute.top
Ocircumflex: O circumflex.top
Otilde: O tilde.top
Odieresis: O dieresis.top
Ugrave: U grave.top
Uacute: U acute.top
Ucircumflex: U circumflex.top
Udieresis: U dieresis.top
Yacute: Y acute.top
Ydieresis: Y dieresis.top
agrave: a grave.top
aacute: a acute.top
acircumflex: a circumflex.top
atilde: a tilde.top
adieresis: a dieresis.top
aring: a ring.ring
ccedilla: c cedilla.cedilla
egrave: e grave.top
eacute: e acute.top
ecircumflex: e circumflex.top
edieresis: e dieresis.top
igrave: dotlessi grave.top
iacute: dotlessi acute.top
icircumflex: dotlessi circumflex.top
idieresis: dotlessi dieresis.top
ntilde: n tilde.top
ograve: o grave.top
oacute: o acute.top
ocircumflex: o circumflex.top
otilde: o tilde.top
odieresis: o dieresis.top
ugrave: u grave.top
uacute: u acute.top
ucircumflex: u circumflex.top
udieresis: u dieresis.top
yacute: y acute.top
ydieresis: y dieresis.top"""


Latin_Ext_A="""Amacron: A macron.top
amacron: a macron.top
Abreve: A breve.top
abreve: a breve.top
Aogonek: A ogonek.ogonek
aogonek: a ogonek.ogonek
Cacute: C acute.top
cacute: c acute.top
Ccircumflex: C circumflex.top
ccircumflex: c circumflex.top
Cdotaccent: C dotaccent.top
cdotaccent: c dotaccent.top
Ccaron: C caron.top
ccaron: c caron.top
Dcaron: D caron.top
dcaron: d caronslovak.right
Emacron: E macron.top
emacron: e macron.top
Ebreve: E breve.top
ebreve: e breve.top
Edotaccent: E dotaccent.top
edotaccent: e dotaccent.top
Eogonek: E ogonek.ogonek
eogonek: e ogonek.ogonek
Ecaron: E caron.top
ecaron: e caron.top
Gcircumflex: G circumflex.top
gcircumflex: g circumflex.top
Gbreve: G breve.top
gbreve: g breve.top
Gdotaccent: G dotaccent.top
gdotaccent: g dotaccent.top
Gcommaaccent: G commaaccent.bottom
gcommaaccent: g commaaccent_top.top
Hcircumflex: H circumflex.top
hcircumflex: h circumflex.top
Itilde: I tilde.top
itilde: dotlessi tilde.top
Imacron: I macron.top
imacron: dotlessi macron.top
Ibreve: I breve.top
ibreve: dotlessi breve.top
Iogonek: I ogonek.ogonek
iogonek: i ogonek.ogonek
Idotaccent: I dotaccent.top
Jcircumflex: J circumflex.top
jcircumflex: dotlessj circumflex.top
Kcommaaccent: K commaaccent.bottom
kcommaaccent: k commaaccent.bottom
Lacute: L acute.top
lacute: l acute.top
Lcedilla: L commaaccent.bottom
lcedilla: l commaaccent.bottom
Lcaron: L caronslovak.right
lcaron: l caronslovak.right
Ldot: L dotaccent.middle
ldot: l dotaccent.middle
Nacute: N acute.top
nacute: n acute.top
Ncommaaccent: N commaaccent.bottom
ncommaaccent: n commaaccent.bottom
Ncaron: N caron.top
ncaron: n caron.top
Omacron: O macron.top
omacron: o macron.top
Obreve: O breve.top
obreve: o breve.top
Ohungarumlaut: O hungarumlaut.top
ohungarumlaut: o hungarumlaut.top
Racute: R acute.top
racute: r acute.top
Rcommaaccent: R commaaccent.bottom
rcommaaccent: r commaaccent.bottom
Rcaron: R caron.top
rcaron: r caron.top
Sacute: S acute.top
sacute: s acute.top
Scircumflex: S circumflex.top
scircumflex: s circumflex.top
Scedilla: S cedilla.bottom
scedilla: s cedilla.bottom
Scaron: S caron.top
scaron: s caron.top
Tcommaaccent: T commaaccent.bottom
tcommaaccent: t commaaccent.bottom
Tcaron: T caron.top
tcaron: t caronslovak.right
Utilde: U tilde.top
utilde: u tilde.top
Umacron: U macron.top
umacron: u macron.top
Ubreve: U breve.top
ubreve: u breve.top
Uring: U ring.top
uring: u ring.top
Uhungarumlaut: U hungarumlaut.top
uhungarumlaut: u hungarumlaut.top
Uogonek: U ogonek.ogonek
uogonek: u ogonek.ogonek
Wcircumflex: W circumflex.top
wcircumflex: w circumflex.top
Ycircumflex: Y circumflex.top
ycircumflex: y circumflex.top
Zacute: Z acute.top
zacute: z acute.top
Zdotaccent: Z dotaccent.top
zdotaccent: z dotaccent.top
Zcaron: Z caron.top
zcaron: z caron.top"""

Latin_Ext_B="""Ohorn: O horn.horn
ohorn: o horn.horn
Uhorn: U horn.horn
uhorn: u horn.horn
Acaron: A caron.top
acaron: a caron.top
Icaron: I caron.top
icaron: dotlessi caron.top
Ocaron: O caron.top
ocaron: o caron.top
Ucaron: U caron.top
ucaron: u caron.top
#Udieresismacron: U dieresismacron.top
#udieresismacron: u dieresismacron.top
Udieresisacute: U dieresisacute.top
udieresisacute: u dieresisacute.top
Udieresiscaron: U dieresiscaron.top
udieresiscaron: u dieresiscaron.top
Udieresisgrave: U dieresisgrave.top
udieresisgrave: u dieresisgrave.top
Adieresismacron: A dieresismacron.top
adieresismacron: a dieresismacron.top
Adotmacron: A dotmacron.top
adotmacron: a dotmacron.top
AEmacron: AE macron.top
aemacron: ae macron.top
Gcaron: G caron.top
gcaron: g caron.top
Kcaron: K caron.top
kcaron: k caron.top
Oogonek: O ogonek.ogonek
oogonek: o ogonek.ogonek
Oogonekmacron: O ogonek.ogonek macron.top
oogonekmacron: o ogonek.ogonek macron.top
Jcaron: J caron.top
jcaron: dotlessj caron.top
Gacute: G acute.top
gacute: g acute.top
Aringacute: A ringacute.ring
aringacute: a ringacute.top
AEacute: AE acute.top
aeacute: ae acute.top
Oslashacute: Oslash acute.top
oslashacute: oslash acute.top
Adblgrave: A dblgrave.top
adblgrave: a dblgrave.top
Ainvertedbreve: A invertedbreve.top
ainvertedbreve: a invertedbreve.top
Edblgrave: E dblgrave.top
edblgrave: e dblgrave.top
Einvertedbreve: E invertedbreve.top
einvertedbreve: e invertedbreve.top
Idblgrave: I dblgrave.top
idblgrave: dotlessi dblgrave.top
Iinvertedbreve: I invertedbreve.top
iinvertedbreve: dotlessi invertedbreve.top
Odblgrave: O dblgrave.top
odblgrave: o dblgrave.top
Oinvertedbreve: O invertedbreve.top
oinvertedbreve: o invertedbreve.top
Rdblgrave: R dblgrave.top
rdblgrave: r dblgrave.top
Rinvertedbreve: R invertedbreve.top
rinvertedbreve: r invertedbreve.top
Udblgrave: U dblgrave.top
udblgrave: u dblgrave.top
Uinvertedbreve: U invertedbreve.top
uinvertedbreve: u invertedbreve.top
Scommaaccent: S commaaccent.bottom
scommaaccent: s commaaccent.bottom
Hcaron: H caron.top
hcaron: h caron.top
Adotaccent: A dotaccent.top
adotaccent: a dotaccent.top
Ecedilla: E cedilla.bottom
ecedilla: e cedilla.bottom"""

Latin_Ext_additional = """Aringbelow: A ring.bottom
aringbelow: a ring.bottom
Bdotaccent: B dotaccent.top
bdotaccent: b dotaccent.top
Bdotbelow: B dotaccent.bottom
bdotbelow: b dotaccent.bottom
Blinebelow: B macron.bottom
blinebelow: b macron.bottom
Ccedillaacute: C cedilla.cedilla acute.top
ccedillaacute: c cedilla.cedilla acute.top
Ddotaccent: D dotaccent.top
ddotaccent: d dotaccent.top
Ddotbelow: D dotaccent.bottom
ddotbelow: d dotaccent.bottom
Dlinebelow: D macron.bottom
dlinebelow: d macron.bottom
Dcedilla: D cedillacomma.bottom
dcedilla: d commaaccent.bottom
Dcircumflexbelow: D circumflex.bottom
dcircumflexbelow: d circumflex.bottom
Emacrongrave: E macrongrave.top
emacrongrave: e macrongrave.top
Emacronacute: E macronacute.top
emacronacute: e macronacute.top
Ecircumflexbelow: E circumflex.bottom
ecircumflexbelow: e circumflex.bottom
Etildebelow: E tilde.bottom
etildebelow: e tilde.bottom
Ecedillabreve: E cedilla.bottom breve.top
ecedillabreve: e cedilla.bottom breve.top
Fdotaccent: F dotaccent.top
fdotaccent: f dotaccent.top
Gmacron: G macron.top
gmacron: g macron.top
Hdotaccent: H dotaccent.top
hdotaccent: h dotaccent.top
Hdotbelow: H dotaccent.bottom
hdotbelow: h dotaccent.bottom
Hdieresis: H dieresis.top
hdieresis: h dieresis.top
Hcedilla: H cedilla.cedilla
hcedilla: h cedilla.cedilla
Hbrevebelow: H breve.bottom
hbrevebelow: h breve.bottom
Itildebelow: I tilde.bottom
itildebelow: i tilde.bottom
Idieresisacute: I dieresisacute.top
idieresisacute: dotlessi dieresisacute.top
Kacute: K acute.top
kacute: k acute.top
Kdotbelow: K dotaccent.bottom
kdotbelow: k dotaccent.bottom
Klinebelow: K macron.bottom
klinebelow: k macron.bottom
Ldotbelow: L dotaccent.bottom
ldotbelow: l dotaccent.bottom
Ldotbelowmacron: L dotaccent.bottom macron.top
ldotbelowmacron: l dotaccent.bottom macron.top
Llinebelow: L macron.bottom
llinebelow: l macron.bottom
Lcircumflexbelow: L circumflex.bottom
lcircumflexbelow: l circumflex.bottom
Macute: M acute.top
macute: m acute.top
Mdotaccent: M dotaccent.top
mdotaccent: m dotaccent.top
Mdotbelow: M dotaccent.bottom
mdotbelow: m dotaccent.bottom
Ndotaccent: N dotaccent.top
ndotaccent: n dotaccent.top
Ndotbelow: N dotaccent.bottom
ndotbelow: n dotaccent.bottom
Nlinebelow: N macron.bottom
nlinebelow: n macron.bottom
Ncircumflexbelow: N circumflex.bottom
ncircumflexbelow: n circumflex.bottom
Otildeacute: O tildeacute.top
otildeacute: o tildeacute.top
Otildedieresis: O tildedieresis.top
otildedieresis: o tildedieresis.top
Omacrongrave: O macrongrave.top
omacrongrave: o macrongrave.top
Omacronacute: O macronacute.top
omacronacute: o macronacute.top
Pacute: P acute.top
pacute: p acute.top
Pdotaccent: P dotaccent.top
pdotaccent: p dotaccent.top
Rdotaccent: R dotaccent.top
rdotaccent: r dotaccent.top
Rdotbelow: R dotaccent.bottom
rdotbelow: r dotaccent.bottom
Rdotbelowmacron: R dotaccent.bottom macron.top
rdotbelowmacron: r dotaccent.bottom macron.top
Rlinebelow: R macron.bottom
rlinebelow: r macron.bottom
Sdotaccent: S dotaccent.top
sdotaccent: s dotaccent.top
Sdotbelow: S dotaccent.bottom
sdotbelow: s dotaccent.bottom
Sacutedotaccent: S acutedotaccent.top
sacutedotaccent: s acutedotaccent.top
Scarondotaccent: S carondotaccent.top
scarondotaccent: s carondotaccent.top
Sdotbelowdotaccent: S dotaccent.bottom dotaccent.top
sdotbelowdotaccent: s dotaccent.bottom dotaccent.top
Tdotaccent: T dotaccent.top
tdotaccent: t dotaccent.top
Tdotbelow: T dotaccent.bottom
tdotbelow: t dotaccent.bottom
Tlinebelow: T macron.bottom
tlinebelow: t macron.bottom
Tcircumflexbelow: T circumflex.bottom
tcircumflexbelow: t circumflex.bottom
Udieresisbelow: U dieresis.bottom
udieresisbelow: u dieresis.bottom
Utildebelow: U tilde.bottom
utildebelow: u tilde.bottom
Ucircumflexbelow: U circumflex.bottom
ucircumflexbelow: u circumflex.bottom
Utildeacute: U tildeacute.top
utildeacute: u tildeacute.top
Umacrondieresis: U macrondieresis.top
umacrondieresis: u macrondieresis.top
Vtilde: V tilde.top
vtilde: v tilde.top
Vdotbelow: V dotaccent.bottom
vdotbelow: v dotaccent.bottom
Wgrave: W grave.top
wgrave: w grave.top
Wacute: W acute.top
wacute: w acute.top
Wdieresis: W dieresis.top
wdieresis: w dieresis.top
Wdotaccent: W dotaccent.top
wdotaccent: w dotaccent.top
Wdotbelow: W dotaccent.bottom
wdotbelow: w dotaccent.bottom
Xdotaccent: X dotaccent.top
xdotaccent: x dotaccent.top
Xdieresis: X dieresis.top
xdieresis: x dieresis.top
Ydotaccent: Y dotaccent.top
ydotaccent: y dotaccent.top
Zcircumflex: Z circumflex.top
zcircumflex: z circumflex.top
Zdotbelow: Z dotaccent.bottom
zdotbelow: z dotaccent.bottom
Zlinebelow: Z macron.bottom
zlinebelow: z macron.bottom
hlinebelow: h macron.bottom
tdieresis: t dieresis.top
wring: w ring.top
yring: y ring.top
arighthalfring: a righthalfring.top
longsdotaccent: longs dotaccent.top
Adotbelow: A dotaccent.bottom
adotbelow: a dotaccent.bottom
Ahookabove: A hookabove.top
ahookabove: a hookabove.top
Acircumflexacute: A circumflexacute.top
acircumflexacute: a circumflexacute.top
Acircumflexgrave: A circumflexgrave.top
acircumflexgrave: a circumflexgrave.top
Acircumflexhookabove: A circumflexhookabove.top
acircumflexhookabove: a circumflexhookabove.top
Acircumflextilde: A circumflextilde.top
acircumflextilde: a circumflextilde.top
Acircumflexdotbelow: A circumflex.top dotaccent.bottom
acircumflexdotbelow: a circumflex.top dotaccent.bottom
Abreveacute: A breveacute.top
abreveacute: a breveacute.top
Abrevegrave: A brevegrave.top
abrevegrave: a brevegrave.top
Abrevehookabove: A brevehookabove.top
abrevehookabove: a brevehookabove.top
Abrevetilde: A brevetilde.top
abrevetilde: a brevetilde.top
Abrevedotbelow: A breve.top dotaccent.bottom
abrevedotbelow: a breve.top dotaccent.bottom
Edotbelow: E dotaccent.bottom
edotbelow: e dotaccent.bottom
Ehookabove: E hookabove.top
ehookabove: e hookabove.top
Etilde: E tilde.top
etilde: e tilde.top
Ecircumflexacute: E circumflexacute.top
ecircumflexacute: e circumflexacute.top
Ecircumflexgrave: E circumflexgrave.top
ecircumflexgrave: e circumflexgrave.top
Ecircumflexhookabove: E circumflexhookabove.top
ecircumflexhookabove: e circumflexhookabove.top
Ecircumflextilde: E circumflextilde.top
ecircumflextilde: e circumflextilde.top
Ecircumflexdotbelow: E circumflex.top dotaccent.bottom
ecircumflexdotbelow: e circumflex.top dotaccent.bottom
Ihookabove: I hookabove.top
ihookabove: dotlessi hookabove.top
Idotbelow: I dotaccent.bottom
idotbelow: i dotaccent.bottom
Odotbelow: O dotaccent.bottom
odotbelow: o dotaccent.bottom
Ohookabove: O hookabove.top
ohookabove: o hookabove.top
Ocircumflexacute: O circumflexacute.top
ocircumflexacute: o circumflexacute.top
Ocircumflexgrave: O circumflexgrave.top
ocircumflexgrave: o circumflexgrave.top
Ocircumflexhookabove: O circumflexhookabove.top
ocircumflexhookabove: o circumflexhookabove.top
Ocircumflextilde: O circumflextilde.top
ocircumflextilde: o circumflextilde.top
Ocircumflexdotbelow: O circumflex.top dotaccent.bottom
ocircumflexdotbelow: o circumflex.top dotaccent.bottom
Ohornacute: O horn.horn acute.top
ohornacute: o horn.horn acute.top
Ohorngrave: O horn.horn grave.top
ohorngrave: o horn.horn grave.top
Ohornhookabove: O horn.horn hookabove.top
ohornhookabove: o horn.horn hookabove.top
Ohorntilde: O horn.horn tilde.top
ohorntilde: o horn.horn tilde.top
Ohorndotbelow: O horn.horn dotaccent.bottom
ohorndotbelow: o horn.horn dotaccent.bottom
Udotbelow: U dotaccent.bottom
udotbelow: u dotaccent.bottom
Uhookabove: U hookabove.top
uhookabove: u hookabove.top
Uhornacute: U horn.horn acute.top
uhornacute: u horn.horn acute.top
Uhorngrave: U horn.horn grave.top
uhorngrave: u horn.horn grave.top
Uhornhookabove: U horn.horn hookabove.top
uhornhookabove: u horn.horn hookabove.top
Uhorntilde: U horn.horn tilde.top
uhorntilde: u horn.horn tilde.top
Uhorndotbelow: U horn.horn dotaccent.bottom
uhorndotbelow: u horn.horn dotaccent.bottom
Ygrave: Y grave.top
ygrave: y grave.top
Ydotbelow: Y dotaccent.bottom
ydotbelow: y dotaccent.bottom
Yhookabove: Y hookabove.top
yhookabove: y hookabove.top
Ytilde: Y tilde.top
ytilde: y tilde.top"""

alles = """"""
alles += Latin_Supp
alles += "\n" + Latin_Ext_A
alles += "\n" + Latin_Ext_B
alles += "\n" + Latin_Ext_additional
#alles += "\n" + Testing

_blocks = ["Latin_Supp", "Latin_Ext_A", "Latin_Ext_B", "Latin_Ext_additional"]

#Accents =""""""

