from robofab import *
from mojo.UI import *
from mojo.events import addObserver, removeObserver
from mojo.drawingTools import *
from vanilla import *
from lib.tools.defaults import getDefault, setDefault
from lib.tools.notifications import PostNotification
from mojo.extensions import getExtensionDefault, setExtensionDefault


title ="Name" # Keep them unique!
event = "drawBackground" 
turnOffItems=['Metrics', 'Metrics Title'] 
settingsIsOpen = "%sSettingsKey" % title

# dont edit
selfKey = "%sKey" % title
wasOn = getExtensionDefault(selfKey)
# dont change name of class
class ThisObserver(object):
		
	def __init__(self, active=bool):
		self.active = active
		if getExtensionDefault(selfKey):
			if self.active == True:
				return
			if self.active == False:
				pass
		
		if self.active == True:
			self._activate()
			self._turnOff()
		if self.active == False:
			self._end()

		
	# dont change names of functions
	# dont edit _functions!
	# but you can add functions if needed


	def _activate(self):	
		setExtensionDefault(selfKey, self)
		addObserver(self, "mainFunction", event)

	def _end(self):
		print "end"
		removeObserver(getExtensionDefault(selfKey),event)
		setExtensionDefault(selfKey, None)
		# this is optional
		if wasOn:
			self._restoreGlyphViewItems()
		self.w.hide()
		setExtensionDefault(settingsIsOpen, False)
	
	def mainFunction(self, info):
		if not getExtensionDefault(settingsIsOpen):
			self.settingsWindow()
	
		g=info["glyph"]
		f=g.getParent()
		stroke(0)
		strokeWidth(1)
		fill(None)
		line(-50,0,g.width+50,0)
		line(0,0,0,50)
		line(g.width,0,g.width,50)

	def settingsWindow(self):
		global w
		self.w = Window((100,300),"name")
		self.w.open()
		setExtensionDefault(settingsIsOpen, True)
			
	def _turnOff(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				pref_new[i] = 0
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")

	def _restoreGlyphViewItems(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				pref_new[i] = 1
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")