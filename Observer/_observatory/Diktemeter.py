
# TODO:
# 	make the _contour glyph invisible...
# 	add more settings:
# 						miterLimit,
# 						add components?




from robofab import *
from mojo.UI import *
from mojo.events import addObserver, removeObserver
from mojo.drawingTools import *
from vanilla import *
from lib.tools.defaults import getDefault, setDefault
from lib.tools.notifications import PostNotification
from mojo.extensions import getExtensionDefault, setExtensionDefault
from mojo.roboFont import CurrentFont


title ="Diktemeter" # Keep them unique!
event = "drawBackground" 
turnOffItems=[] 
settingsWindow = "%sSettingsKey" % title

# dont edit
selfKey = "%sKey" % title
wasOn = getExtensionDefault(selfKey)

# dont change name of class
class ThisObserver(object):
		
	def __init__(self, active=bool):
		self.active = active
		if getExtensionDefault(selfKey):
			if self.active == True:
				return
			if self.active == False:
				pass
		
		if self.active == True:
			self._activate()
			self.turnOff()
		if self.active == False:
			self._end()

		
	# dont change names of functions
	# dont edit _functions!
	# but you can add functions if needed


	def _activate(self):	
		setExtensionDefault(selfKey, self)
		addObserver(self, "mainFunction", event)

	def _end(self):
		removeObserver(getExtensionDefault(selfKey),event)
		setExtensionDefault(selfKey, None)
		# this is optional
		if wasOn:
			self.restoreGlyphViewItems()
		#kill settings window
		self.settingsWindow(onOff=False)
		f = CurrentFont()
		try:f.removeGlyph("_contour")
		except:pass
		stroke(None)
	
	def mainFunction(self, info):
		if not getExtensionDefault(settingsWindow):
			self.settingsWindow(onOff=True)
		
		self.w = getExtensionDefault(settingsWindow)	

		stroke(1,0,0,.8)
		strokeWidth(int(self.w.edit.get()))
		miterLimit(3)
		fill(None)
		
		g = info['glyph']
		f = g.getParent()
		f.insertGlyph(g, name="_contour")
		g = f["_contour"]
		
		#for component in g.components:
		# 	for contour in f[component.baseGlyph].contours:
		#		g.appendGlyph(f[component.baseGlyph], (component.offset[0], component.offset[1]))
		#		try:g.removeComponent(component)
		#		except:pass
		g.decompose()
		g.removeOverlap()
		drawGlyph(g)

	
	def settingsWindow(self, onOff=bool):
		if onOff == True:
			self.w = FloatingWindow((100,70),"Diktemeter", closable = False)
			setExtensionDefault(settingsWindow, self.w)
			
			self.w.edit = EditText((7,7,-7,23), '30', callback=self.updateView)
			self.w.slider = Slider((7,35,-7,23), minValue=10, maxValue=100,callback=self.setWidth)
			self.w.open()

		if onOff == False:
			try:
				getExtensionDefault(settingsWindow).hide()
				setExtensionDefault(settingsWindow, None)
			except:
				pass
	def updateView(self, sender):
		PostNotification('doodle.updateGlyphView')
	def setWidth(self, sender):
		self.w.edit.set(int(sender.get()))
		self.updateView(None)
		
	def turnOff(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				#global gridWasOn
				#gridWasOn = pref_as_is[i]
				pref_new[i] = 0
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")

	def restoreGlyphViewItems(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				#global gridWasOn
				#gridWasOn = pref_as_is[i]
				pref_new[i] = 1
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")