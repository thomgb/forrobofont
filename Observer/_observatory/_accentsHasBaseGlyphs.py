import _glyphConstructionCategories
reload(_glyphConstructionCategories)
from _glyphConstructionCategories import *

def accentsHasBaseGlyphs(myset):
	accentsHasBaseGlyphs = {}

	for i in myset.split("\n"):
		for j in i.split():
			if "." in j:
				accentsHasBaseGlyphs[j.split(".")[0]] = []

	for i in myset.split("\n"):
		baseGlyph = i.split()[1]
		for j in i.split():
			if "." in j:
				accentsHasBaseGlyphs[j.split(".")[0]].append(baseGlyph)
				accentsHasBaseGlyphs[j.split(".")[0]] = list(set(accentsHasBaseGlyphs[j.split(".")[0]]))
	return accentsHasBaseGlyphs


