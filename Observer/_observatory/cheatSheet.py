from robofab import *
from mojo.UI import *
from mojo.events import addObserver, removeObserver
from mojo.drawingTools import *
from vanilla import *
from lib.tools.defaults import getDefault, setDefault
from lib.tools.notifications import PostNotification
from mojo.extensions import getExtensionDefault, setExtensionDefault


title ="CheatSheet"
event = "drawBackground" 
turnOffItems=[]

# dont edit
selfKey = "%sKey" % title
wasOn = getExtensionDefault(selfKey)
# dont change name of class
class ThisObserver(object):
		
	def __init__(self, active=bool):
		self.active = active
		if getExtensionDefault(selfKey):
			if self.active == True:
				return
			if self.active == False:
				pass
		
		if self.active == True:
			self._activate()
			self._turnOff()
		if self.active == False:
			self._end()

		
	# dont change names of functions
	# dont edit _functions!
	# but you can add functions if needed


	def _activate(self):	
		setExtensionDefault(selfKey, self)
		addObserver(self, "mainFunction", event)

	def _end(self):
		removeObserver(getExtensionDefault(selfKey),event)
		setExtensionDefault(selfKey, None)
		# this is optional
		if wasOn:
			self._restoreGlyphViewItems()
	
	def mainFunction(self, info):
		g = info["glyph"]
		f = g.getParent()
		fill(0.34, 0.71, 0.14,1)
		fontSize(48)
		#texts
		#text("%sg: %s" % (unichr(189), str(g.width/2)), (1.1*g.width, f.info.capHeight))
		
		#draw
		strokeWidth(1)
		stroke(0.34, 0.71, 0.14,1)
		line(g.width/2, f.info.descender, g.width/2, f.info.ascender)
		strokeWidth(None)




	def _turnOff(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				#global gridWasOn
				#gridWasOn = pref_as_is[i]
				pref_new[i] = 0
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")

	def _restoreGlyphViewItems(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				#global gridWasOn
				#gridWasOn = pref_as_is[i]
				pref_new[i] = 1
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")