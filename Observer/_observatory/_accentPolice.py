# coding=utf-8
# TODO:
# 	make the _contour glyph invisible...
# 	add more settings:
# 						miterLimit,
# 						add components?




from robofab.tools.toolsAll import readGlyphConstructions as rGC
from AppKit import NSColor
from vanilla import *
from defconAppKit.windows.baseWindow import BaseWindowController
from mojo.events import addObserver, removeObserver
from mojo.drawingTools import *
from mojo.extensions import getExtensionDefault, setExtensionDefault
from mojo.UI import UpdateCurrentGlyphView
from lib.tools.defaults import getDefault, setDefault
from lib.tools.notifications import PostNotification
from mojo.roboFont import CurrentFont, CurrentGlyph
from lib.cells.colorCell import ColorCell
import _glyphConstructionCategories
reload(_glyphConstructionCategories)
from _glyphConstructionCategories import *

from random import randint


title ="Accents Controle" # Keep them unique!
event = "draw" 
turnOffItems=[] 
settingsWindow = "%sSettingsKey" % title
defaultKey = "nl.geenbitter.accentOverlay"

# dont edit
selfKey = "%sKey" % title
wasOn = getExtensionDefault(selfKey)

pijlen = {
	'top': u'\u2191',
	'bottom': unichr(8595),
	'below': unichr(8615),
	'right': u"r",
	'middle': u"m",
	'bar': u"–",
	'ogonek': u'o',
	}

# glyphConstructions = readGlyphConstructions()
# print glyphConstructions

# reverseGlyphConstructions= dict()
# for accentGlyph, construction in glyphConstructions.items():
# 	if '.sc' in construction[0]:
# 		baseGlyph = construction[0].lower()
# 	else:
# 		baseGlyph = construction[0]
	
# 	parts = construction[1:]
# 	if baseGlyph not in reverseGlyphConstructions:
# 		reverseGlyphConstructions[baseGlyph] = dict()
	
# 	for accentGlyph, position in parts:
# 		if position not in reverseGlyphConstructions[baseGlyph]:
# 			reverseGlyphConstructions[baseGlyph][position] = set()
		
# 		reverseGlyphConstructions[baseGlyph][position].add(accentGlyph)

#print reverseGlyphConstructions

def readGlyphConstructions(mySet=alles):
	"""read GlyphConstruction and turn it into a dict"""
	data = mySet.split("\n")
	glyphConstructions = {}
	for i in data:
		if len(i) == 0: continue
		if i[0] != '#':
			name = i.split(': ')[0]
			construction = i.split(': ')[1].split(' ')
			build = [construction[0]]
			for c in construction[1:]:
				accent = c.split('.')[0]
				position = c.split('.')[1]
				build.append((accent, position))
			glyphConstructions[name] = build
	#return glyphConstructions
	reverseGlyphConstructions= dict()
	for accentGlyph, construction in glyphConstructions.items():
		if '.sc' in construction[0]:
			baseGlyph = construction[0].lower()
		else:
			baseGlyph = construction[0]
		
		parts = construction[1:]
		if baseGlyph not in reverseGlyphConstructions:
			reverseGlyphConstructions[baseGlyph] = dict()
		
		for accentGlyph, position in parts:
			if position not in reverseGlyphConstructions[baseGlyph]:
				reverseGlyphConstructions[baseGlyph][position] = set()
			
			reverseGlyphConstructions[baseGlyph][position].add(accentGlyph)
	return reverseGlyphConstructions

Latin_Supp_block = readGlyphConstructions(Latin_Supp)
#print Latin_Supp_block
Latin_Ext_A_block = readGlyphConstructions(Latin_Ext_A)
Latin_Ext_B_block = readGlyphConstructions(Latin_Ext_B)
Latin_Ext_additional_block = readGlyphConstructions(Latin_Ext_additional)

# 
alles_block = readGlyphConstructions(alles)
# for i in alles_block:
# 	print i, alles_block[i]
# 	print 
# print Latin_Supp_block 
# for i in Latin_Supp_block:
# 	print i
# 	print Latin_Supp_block[i]
# print
# print Latin_Ext_A_block
# print
# print Latin_Ext_B_block 
# print
# print Latin_Ext_additional_block 


# dont change name of class
class ThisObserver(BaseWindowController):
		
	def __init__(self, active=bool):
		self.active = active
		if getExtensionDefault(selfKey):
			if self.active == True:
				return
			if self.active == False:
				pass
		
		if self.active == True:
			self._activate()
			self.turnOff()
		if self.active == False:
			self._end()

		
	# dont change names of functions
	# dont edit _functions!
	# but you can add functions if needed


	def _activate(self):	
		setExtensionDefault(selfKey, self)
		addObserver(self, "mainFunction", event)
		addObserver(self, "glyphChange", "currentGlyphChanged")
		addObserver(self, "drawPreviewAccents", "drawPreview")


	def _end(self):
		removeObserver(getExtensionDefault(selfKey),event)
		removeObserver(getExtensionDefault(selfKey),"currentGlyphChanged")
		removeObserver(getExtensionDefault(selfKey),"drawPreview")

		setExtensionDefault(selfKey, None)
		# this is optional
		if wasOn:
			self.restoreGlyphViewItems()
		#kill settings window
		self.settingsWindow(onOff=False)
	
	def mainFunction(self, info):
		if not getExtensionDefault(settingsWindow):
			self.settingsWindow(onOff=True)
		
		self.w = getExtensionDefault(settingsWindow)	
		self.drawAccents(info)
	def settingsWindow(self, onOff=bool):
		if onOff == True:
			self.w = FloatingWindow((200, 300), title, closable=False)
			setExtensionDefault(settingsWindow, self.w)
			#print getExtensionDefault(settingsWindow)
			columnDescriptions = [
								  dict(title="", key="checkBox", cell=CheckBoxListCell(), width=15),
								  dict(title="accent",editable=False),
								  dict(title="anchor",width=60,editable=False),


								  ]
			
			self.w.latin_supp = CheckBox((5,2,35,17),"supp",sizeStyle='mini', callback=self.checkSupp)
			self.w.latin_A = CheckBox((40,2,35,17),"ex a",sizeStyle='mini', callback=self.buildAccentList)
			self.w.latin_B = CheckBox((75,2,35,17),"ex b",sizeStyle='mini', callback=self.buildAccentList)
			self.w.latin_add = CheckBox((110,2,35,17),"add",sizeStyle='mini', callback=self.buildAccentList)

			self.w.allOn = SquareButton((0,17,-75,17),"all on", callback=self.allOn, sizeStyle='mini')
			self.w.allOff = SquareButton((75,17,-0,17),"all off", callback=self.allOff, sizeStyle='mini')
			self.w.list = List((0, 17+17, -0, -20),
							   items=[], 
							   columnDescriptions=columnDescriptions,
							   editCallback=self.listEditCallback,
							   )
			
			
			self.w.compile = SquareButton((0,-20,-0,-0),'compile these glyphs', callback=self.compileGlyphs, sizeStyle='small')
			self.setUpBaseWindowBehavior()
			self.w.open()
			self.buildAccentList()
		
		if onOff == False:
			try:
				getExtensionDefault(settingsWindow).hide()
				setExtensionDefault(settingsWindow, None)
			except:
				pass
	
	def compileGlyphs(self, sender):
		glyph = CurrentGlyph().name
		font = CurrentFont()
		items = self.w.list.get()
		compositionDict = rGC()
		for item in items:
			if not item["checkBox"]:
				continue
			for compo in compositionDict:
				baseGlyph = compositionDict[compo][0]
				accentCompo = compositionDict[compo][1:]
				if baseGlyph == glyph:
					if compositionDict[compo][1][0] == item["accent"] and len(compositionDict[compo])==2:
						try:
							font.removeGlyph(com)
						except:
							pass
						font.compileGlyph(compo, baseName=baseGlyph, accentNames=accentCompo)
						font[compo].mark = (0.96, 0.82, 0.79, 1)


	def allOn(self, sender):
		for i in self.w.list.get():
			i['checkBox']=True
	def allOff(self, sender):
		for i in self.w.list.get():
			i['checkBox']=False

	def checkSupp(self, sender):
		
		g=CurrentGlyph()
		defaults = getExtensionDefault(defaultKey, dict())
		# print Latin_Supp_block
		# print
		# print defaults
		# print

		for glyphName in Latin_Supp_block:
			if glyphName == g.name:
				# g.name
				for anchorName in Latin_Supp_block[glyphName]:
					# top
					for accent in Latin_Supp_block[glyphName][anchorName]:
						# grave
						for listItem in self.w.list.get():
							
							for pijl in pijlen.items():
								if listItem["anchor"] == pijl[1]:
									anchor = pijl[0]
									if anchor == anchorName and accent == listItem['accent']:
										listItem['checkBox'] = True
									


	def listEditCallback(self, sender):
		## save to defaults
		defaults = getExtensionDefault(defaultKey, dict())
		# print defaults
		for item in sender:
			

			## anchor position
			for pijl in pijlen.items():
				if item["anchor"] == pijl[1]:
					anchor = pijl[0]
			##
			
			accentPosition = "%s.%s" % (item["accent"], anchor)
			defaults[accentPosition] = item["checkBox"]
			
		setExtensionDefault(defaultKey, defaults)
		UpdateCurrentGlyphView()
	
	def buildAccentList(self, sender=None):
		try:
			self.w = getExtensionDefault(settingsWindow)
			## welke unicode blokken?
			accentsDict = alles_block
			

			# if self.w.latin_supp.get():
			# 	accentsDict = dict(accentsDict.items() + Latin_Supp_block.items())
			
			# if self.w.latin_A.get():
			# 	# lege lijst, voeg alles toe
			# 	if len(accentsDict) == 0:
			# 		accentsDict = Latin_Ext_A_block
			# 	# 
			# 	else:
			# 		for glyph in Latin_Ext_A_block:
			# 			# voeg alleen niet bestaande glyphs toe
			# 			if glyph not in accentsDict:
			# 				accentsDict[glyph] = Latin_Ext_A_block[glyph]
			# 			# glyph wel al in lijst
			# 			elif glyph in accentsDict:
			# 				# anchor nog niet bestaand
			# 				for anchor in Latin_Ext_A_block[glyph]:
			# 					print anchor
			# 					if anchor in accentsDict[glyph]:
			# 						print 'wel',anchor
			# 						#accentsDict[glyph][anchor].append(Latin_Ext_A_block[glyph][anchor])
			# 					else:
			# 						print 'niet',anchor
			# 						print glyph, accentsDict[glyph]
			# 						accentsDict[glyph][anchor] = Latin_Ext_A_block[glyph][anchor]



		
			# print accentsDict

			glyph = CurrentGlyph()
			if glyph is None:
				self.w.setTitle("---")
				self.w.list.set([])
				return
			
			font = glyph.getParent()
			
			self.w.setTitle(glyph.name)
			
			items = []
			
			defaults = getExtensionDefault(defaultKey, dict())
			if glyph.name in accentsDict:
				for position, accentGlyphs in accentsDict[glyph.name].items():
					for accentGlyph in accentGlyphs:
						if accentGlyph in font:

							items.append(dict(
								anchor = pijlen[position], 
								accent = accentGlyph, 
								checkBox = defaults.get("%s.%s"%(accentGlyph,position), True)
								))
					
			self.w.list.set(items)
			#print len(items)
			self.w.resize(160,17+17+17+(19*len(items))+21)
		except:
			pass

	
	## notifications
	
	def glyphChange(self, info):
		self.buildAccentList()
	
	def drawAccents(self, info):
		
		glyph = info["glyph"]
		font = glyph.getParent()
		items = self.w.list.get()

		fill(0, .5, .5, .2)
		stroke(None)
		baseGlyphAnchors = {}
		for a in glyph.anchors:
			baseGlyphAnchors[a.name]=[a.x, a.y]
			
	   
		
		
		for item in items:
			if not item["checkBox"]:
				continue
			accent = item['accent']
			for pijl in pijlen.items():
				if item["anchor"] == pijl[1]:
					anchor = pijl[0]

			if accent in font:
				for baseGlyphAnchor in glyph.anchors:
					for accentAnchor in font[accent].anchors:
						if accentAnchor.name[1:] == anchor: ## zoek hier op elk anchor je moet hebben
							if accentAnchor.name[1:] == baseGlyphAnchor.name:
								save()
								## do the transform here
								x = baseGlyphAnchor.x - accentAnchor.x
								y = baseGlyphAnchor.y - accentAnchor.y
								translate(x,y)
								## INDIVIDUELE KLEUR
								#red = float(randint(0,10))/10
								#green = float(randint(0,10))/10
								#blue = float(randint(0,10))/10
								#print red, green, blue
								#fill(red,green,blue,.2)
								##
								drawGlyph(font[accent])
								restore()
					
	def drawPreviewAccents(self, info):
		self.w = getExtensionDefault(settingsWindow)
		glyph = info["glyph"]
		font = glyph.getParent()
		items = self.w.list.get()
		#print items
		fill(0, 0, 0)

		baseGlyphAnchors = {}
		for a in glyph.anchors:
			baseGlyphAnchors[a.name]=[a.x, a.y]

		
		for item in items:
			if not item["checkBox"]:
				continue
			accent = item['accent']
			for pijl in pijlen.items():
				if item["anchor"] == pijl[1]:
					anchor = pijl[0]

			if accent in font:
				for baseGlyphAnchor in glyph.anchors:
					for accentAnchor in font[accent].anchors:
						if accentAnchor.name[1:] == anchor: ## zoek hier op elk anchor je moet hebben
							if accentAnchor.name[1:] == baseGlyphAnchor.name:
								save()
								## do the transform here
								x = baseGlyphAnchor.x - accentAnchor.x
								y = baseGlyphAnchor.y - accentAnchor.y
								translate(x,y)
								## INDIVIDUELE KLEUR
								drawGlyph(font[accent])
								restore()



	def turnOff(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				#global gridWasOn
				#gridWasOn = pref_as_is[i]
				pref_new[i] = 0
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")

	def restoreGlyphViewItems(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				#global gridWasOn
				#gridWasOn = pref_as_is[i]
				pref_new[i] = 1
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")