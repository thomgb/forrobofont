import os
from mojo.UI import *
from vanilla import *
from vanilla.dialogs import getFile, getFolder
from mojo.events import addObserver, removeObserver
from mojo.extensions import getExtensionDefault, setExtensionDefault
from AppKit import *

from lib.UI.toggleImageButton import ToggleImageButton


defaultKeyObservatory = "nl.thomjanssen.observatory"
_pathObsFolder = "%s.pathObsFolder" % defaultKeyObservatory
_openSettings = "%s.openSettings" % defaultKeyObservatory

OutputWindow().clear()

user = os.getlogin()

pathObsFolder = getExtensionDefault(_pathObsFolder)
pathObsFolder = "/Users/%s/Library/Application Support/RoboFont/scripts/*observer/_obs/" % user
# collect obs
os.chdir(pathObsFolder)
#print os.getcwd()

obsdir = os.listdir(pathObsFolder)
#print obsdir


vink = u' %s' % unichr(10003)
gear = u' %s' % unichr(9881)

# template for ob
listDiscription = dict(checkBox=False, help=False)



class Observertory(object):
	def __init__(self):

		self.o = Window((450,500),"Observertory")
		
		toolbarItems = [
			dict(itemIdentifier="off",
				label="All Off",
				imagePath="/Users/%s/Desktop/nm/allOffIcon.pdf" % user,
				callback=self._allOff,
				),
			dict(itemIdentifier=NSToolbarFlexibleSpaceItemIdentifier),
			dict(itemIdentifier="settings",
				label="Settings",
				imageNamed="prefToolbarMisc",
				callback=self.settings,
				),
			# dict(itemIdentifier="inspector",
			# 	label="Inspector",
			# 	imageNamed="toolbarScriptOpen",
			# 	callback=self.dummy,
			# 	),
			# dict(itemIdentifier=NSToolbarFlexibleSpaceItemIdentifier),
			# dict(itemIdentifier="remove",
			# 	label="Remove",
			# 	imageNamed="toolbarScriptOpen",
			# 	callback=self.dummy,
			# 	),
			# dict(itemIdentifier="removeEmpty",
			# 	label="Remove empty",
			# 	imageNamed="toolbarScriptOpen",
			# 	callback=self.dummy,
			# 	),
			# dict(itemIdentifier=NSToolbarFlexibleSpaceItemIdentifier),
			# dict(itemIdentifier="clipboard",
			# 	label="Clipboard",
			# 	imagePath="toolbarScriptOpen",
			# 	callback=self.dummy,
			# 	),
			# dict(itemIdentifier=NSToolbarFlexibleSpaceItemIdentifier),
			]
		toolbar = self.o.addToolbar(toolbarIdentifier="name", toolbarItems=toolbarItems, addStandardItems=False)




		columnDescriptions = [
							  dict(title=vink, key="checkBox", cell=CheckBoxListCell(), width=15),
							  #dict(title=gear, key="settings", cell=CheckBoxListCell(), width=15),
							  dict(title="observer",key="title",width=385),
							  dict(title="?", key="help",cell=CheckBoxListCell(),)
							]
		self.o.observerList = List((0, 0, -0, -30),
						   items=[], 
						   columnDescriptions=columnDescriptions,
						   editCallback=None,
						   doubleClickCallback= self.doubleClickListCallback,
						   )
		
		self.o.observerList.set(self.makeObserverList())
		
		##
		
		self.o.bind("close", self._close)
		self.o.bind("resigned key",self._resigned)

		self.o.setPosSize((700,250,450,500))		
		self.o.open()
		self.o.move(0,20)

	
	def settings(self,sender):
		setExtensionDefault(_openSettings, True)
		self.s = Sheet((400,140),self.o)
		
		self.s.closeButton = ToggleImageButton((-40,0,40,40), '', bordered=0, imageNamed=NSImageNameStopProgressTemplate, callback=self._settingsClose)

		self.s.putPath = SquareButton((10,20,-50,30),"Put a new folder with observers", callback=self._settingsPutPath)
		#self.s.currentPath = PathControl((10,60,-50,22), getExtensionDefault(_pathObsFolder), sizeStyle="small")
		self.s.currentPath = EditText((10,65,-50,50), getExtensionDefault(_pathObsFolder), sizeStyle="small",readOnly=True)

		self.s.open()

	def _settingsPutPath(self, sender):
		path = getFolder('Where do you store your observers?')[0]
		setExtensionDefault(_pathObsFolder, path)
		self.s.currentPath.set(path)
		

	def _settingsClose(self, sender):
		self.s.close()
		setExtensionDefault(_openSettings, False)

	def doubleClickListCallback(self,sender):
		index = sender.getSelection()[0]
		self.o.observerList.get()[index]['checkBox'] = not self.o.observerList.get()[index]['checkBox']

	def dummy(self,sender):
		pass
	
	def makeObserverList(self):
		myList = list()
		global obsFileNames
		obsFileNames = dict()
		global obsClass
		obsClass = dict()
		for fileName in obsdir:
			if fileName.endswith('.py') and not fileName.startswith('_'):
				ob = fileName[:-3]
				#print ob
				try:
					getName = __import__(ob)
					reload(getName)
					listDiscptionCopy=listDiscription.copy()
					listDiscptionCopy['title']=getName.title
					selfKey = "%sKey" % getName.title
					if getExtensionDefault(selfKey):
						listDiscptionCopy['checkBox'] = True
					myList.append(listDiscptionCopy)
					obsFileNames[getName.title]=fileName
					obsClass[getName.title]=getName
				except:
					continue
		if len(myList) == 0:
			print 'No observers found!'
			listDiscptionCopy=listDiscription.copy()
			listDiscptionCopy['title']='no observers found!'
			myList.append(listDiscptionCopy)

		#myList.sort()
		return myList
	
	def _close(self, sender):
		#print obsFileNames
		#print obsClass
		whatToDO=self.o.observerList.get()
		for ob in whatToDO:
			if ob['checkBox']:
				#print ob['title']
				obsClass[ob['title']].ThisObserver(active = True)
			else:
				#print "not selected: %s" % ob['title']
				obsClass[ob['title']].ThisObserver(active = False)


		#removeObserver(self, event)
		self.o.hide()

	def _resigned(self,sender):
		if getExtensionDefault(_openSettings) == True:
			return
		self._close(None)
		pass

####### TOOLBAR

	def _allOff(self, sender):
		for i in self.o.observerList.get():
			i['checkBox']=False



Observertory()