from robofab import *
from mojo.UI import *
from mojo.events import addObserver, removeObserver
from mojo.drawingTools import *
from vanilla import *
from lib.tools.defaults import getDefault, setDefault
from lib.tools.notifications import PostNotification
from mojo.extensions import getExtensionDefault, setExtensionDefault


title="Minimal Metrics Titles"
selfKey = "%sKey" % title

event = "drawBackground" 

turnOffItems=['Metrics', 'Metrics Titles']
wasOn = getExtensionDefault(selfKey)


class ThisObserver(object):
		
	def __init__(self, active=bool):
		self.active = active
		if getExtensionDefault(selfKey):
			if self.active == True:
				return
			if self.active == False:
				pass
		
		if self.active == True:
			self.activate()
			self._turnOff()
		if self.active == False:
			self.end()

		

	def activate(self):	
		setExtensionDefault(selfKey, self)
		addObserver(self, "mainFunction", event)

	def end(self):
		removeObserver(getExtensionDefault(selfKey),event)
		setExtensionDefault(selfKey, None)
		if wasOn:
			self._restoreGlyphViewItems()
	
	def mainFunction(self, info):
		g=info["glyph"]
		f=g.getParent()
		
		save()
		scale(info['scale']/info['scale'])
		stroke(0)
		strokeWidth(info['scale']/10)
		fill(None)
		line(-50,0,g.width+50,0)
		line(0,0,0,50)
		line(g.width,0,g.width,50)
		

		line(-50,f.info.xHeight,0,f.info.xHeight)
		line(-50,f.info.descender,0,f.info.descender)
		line(-50,f.info.capHeight,0,f.info.capHeight)
		line(-50,f.info.ascender,0,f.info.ascender)
		
		fill(0,0,0,.5)
		stroke(0,0,0,0)
		strokeWidth(0)
		lift = 10
		scaleFactor = 0.03
		save()
		x = -25 - (f['x'].width*scaleFactor/2)
		translate(x,f.info.xHeight+lift)
		scale(scaleFactor)
		drawGlyph(f['x'])
		restore()

		save()
		x = -25 - (f['d'].width*scaleFactor/2)
		translate(x,f.info.descender+lift)
		scale(scaleFactor)
		drawGlyph(f['d'])
		restore()

		save()
		x = -25 - (f['c'].width*scaleFactor/2)
		translate(x,f.info.capHeight+lift)
		scale(scaleFactor)
		drawGlyph(f['c'])
		restore()

		save()
		x = -25 - (f['a'].width*scaleFactor/2)
		translate(x,f.info.ascender+lift)
		scale(scaleFactor)
		drawGlyph(f['a'])
		restore()
		restore()
			
	def _turnOff(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			print i
			if i in turnOffItems:
				#global gridWasOn
				#gridWasOn = pref_as_is[i]
				pref_new[i] = 0
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")

	def _restoreGlyphViewItems(self):
		pref_as_is = getDefault('glyphZoomViewShowItems')
		pref_new = dict()
		for i in pref_as_is:
			if i in turnOffItems:
				#global gridWasOn
				#gridWasOn = pref_as_is[i]
				pref_new[i] = 1
			else:
				pref_new[i] = pref_as_is[i]
		setDefault('glyphZoomViewShowItems', pref_new)
		PostNotification("doodle.preferencesChanged")