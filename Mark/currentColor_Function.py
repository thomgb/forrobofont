from robofab import *
lowercase = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "dotlessi", "dotlessj"]
C =["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AE","OE"]

case = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "dotlessi", "dotlessj", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"]

sym = ["A", "H", "I", "M", "N", "O", "S", "T", "U", "V", "W", "X", "Y", "Z"]

d={}
t=""
l=[]

f = CurrentFont()
g = CurrentGlyph()

color = g.mark

for g in f: 
    
    if g.mark == color:
        g.prepareUndo()
        g.mark = None
        g.performUndo()