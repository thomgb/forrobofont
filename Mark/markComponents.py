
f = CurrentFont()
g = CurrentGlyph()

onlyComp = (0.0, 0.502, 1.0, 1.0)
mixed = (0.6904296875, 0.5446123987203114, 0.3244146695437858, 1.0)
onlyContour = (0,0,0,0)
for g in f:
        g.mark = onlyContour
        for comp in g.components:
            if comp:
                g.mark = onlyComp
                for contour in g.contours:
                    if contour:
                        g.mark = mixed

                
print "done"